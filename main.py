import warnings
warnings.filterwarnings("ignore")
from sklearn.metrics import roc_auc_score, roc_curve
from sksurv.metrics import cumulative_dynamic_auc
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.axes._axes import _log as matplotlib_axes_logger
matplotlib_axes_logger.setLevel('ERROR')
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from functions import memory_management, bool_to_binary
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sksurv.svm import FastSurvivalSVM
from sksurv.util import Surv

train_df = pd.read_csv('training_CustomerChurn-17205389.csv', sep=',')
test_df = pd.read_csv('test_CustomerChurn-17205389.csv', sep=',')
full_df = pd.read_csv('shuffled_CustomerChurn-17205389.csv', sep=',')
categorical_columns = train_df[['income', 'regionType', 'marriageStatus', 'children', 'smartPhone',
                                'creditRating', 'homeOwner', 'creditCard', 'churn']].columns

continuous_columns = train_df[['age', 'numHandsets', 'handsetAge', 'currentHandsetPrice', 'avgBill', 'avgMins',
                               'avgrecurringCharge', 'avgOverBundleMins', 'avgRoamCalls', 'callMinutesChangePct',
                               'billAmountChangePct', 'avgReceivedMins', 'avgOutCalls', 'avgInCalls',
                               'peakOffPeakRatio',
                               'peakOffPeakRatioChangePct', 'avgDroppedCalls', 'lifeTime', 'lastMonthCustomerCareCalls',
                               'numRetentionCalls', 'numRetentionOffersAccepted', 'newFrequentNumbers']].columns

test_df = memory_management(test_df, categorical_columns=categorical_columns, continuous_columns=continuous_columns)
train_df = memory_management(train_df, categorical_columns=categorical_columns, continuous_columns=continuous_columns)
full_df = memory_management(full_df, categorical_columns=categorical_columns, continuous_columns=continuous_columns)

sns.set(style="white")
f, ax = plt.subplots(figsize=(20, 20))

corr = train_df[continuous_columns].corr()

mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

cmap = sns.diverging_palette(225, 0, as_cmap=True)

sns.heatmap(corr, annot=True, mask=mask, cmap=cmap, vmax=1, vmin=-1,
            square=True, xticklabels=True, yticklabels=True,
            linewidths=0.5, cbar_kws={"shrink": 0.5}, ax=ax)
plt.yticks(rotation=0)
plt.xticks(rotation=90)
plt.savefig('heatmap_1.png')

pp = PdfPages('continuous_boxScatterCharts_17205389_1.pdf')
flierprops = dict(marker='o', markerfacecolor='green', markersize=6, linestyle='none')
for i in continuous_columns:
    bp = train_df.boxplot(column=[i], by=['churn'], flierprops=flierprops, figsize=(10, 7))
    pp.savefig(bp.get_figure())

for c in continuous_columns:
    bp = train_df.plot(kind='scatter', x=c, y='churn', label="%.3f" % train_df[[c, 'churn']].corr().values[0, 1])
    pp.savefig(bp.get_figure())
pp.close()

chart_columns = categorical_columns.drop(['churn'], 1)
pp = PdfPages('categorical_stackedBarCharts_17205389.pdf')
for i in chart_columns:
    feature = pd.unique(train_df[i].ravel())
    train_df['percent'] = 0

    for p in feature:
        count = 1 / train_df[train_df[i] == p].count()['churn']
        index_list = train_df[train_df[i] == p].index.tolist()
        for j in index_list:
            train_df.loc[j, 'percent'] = count * 100

    group = train_df[['percent', i, 'churn']].groupby([i, 'churn']).sum()

    my_plot = group.unstack().plot(kind='bar', stacked=True, title="Churn for " + i, figsize=(15, 7))

    green_key = mpatches.Patch(color='orange', label='Churned')
    blue_key = mpatches.Patch(color='blue', label='Didn\'t churn')
    my_plot.legend(handles=[green_key, blue_key], frameon=True)

    my_plot.set_xlabel(i)
    my_plot.set_ylabel("% Churn")
    my_plot.set_ylim([0, 100])
    pp.savefig(my_plot.get_figure())
pp.close()

print(full_df.isnull().sum())
full_df['age'] = full_df['age'].replace(np.nan, 0)
temp_df = full_df[full_df['age'] == 0]
temp_df = temp_df[temp_df['children'] == True]
print(temp_df[['age', 'children']])

temp_df = temp_df[temp_df['homeOwner'] == True]
print(temp_df.head(5)[['age', 'homeOwner']])
print(full_df['age'].median())
full_df['age'] = full_df['age'].replace(0, full_df['age'].median())

print(full_df[full_df['currentHandsetPrice'] == 0]['currentHandsetPrice'].count())
print(full_df[full_df['currentHandsetPrice'] == 0][full_df['avgMins'] == 0][
          ['currentHandsetPrice', 'avgBill', 'avgMins']])
full_df = full_df.drop('currentHandsetPrice', axis=1)

full_df.groupby('regionType')['regionType'].apply(lambda x: x.count())
full_df = full_df.drop('regionType', axis=1)

full_df = full_df.drop('marriageStatus', axis=1)

full_df = full_df.drop('customer', axis=1)
print(full_df)

feature = pd.unique(train_df['creditRating'].ravel())
train_df['percent'] = 0

for p in feature:
    count = 1 / train_df[train_df['creditRating'] == p].count()['churn']
    index_list = train_df[train_df['creditRating'] == p].index.tolist()
    for j in index_list:
        train_df.loc[j, 'percent'] = count * 100

group = train_df[['percent', 'creditRating', 'churn']].groupby(['creditRating', 'churn']).sum()

my_plot = group.unstack().plot(kind='bar', stacked=True, title="Churn for " + 'creditRating', figsize=(15, 7))

green_key = mpatches.Patch(color='orange', label='Churned')
blue_key = mpatches.Patch(color='blue', label='Didn\'t churn')
my_plot.legend(handles=[green_key, blue_key], frameon=True)

my_plot.set_xlabel('creditRating')
my_plot.set_ylabel("% Churn")
my_plot.set_ylim([0, 100])
plt.savefig('creditRating_1.png')

full_df = pd.concat([full_df, pd.get_dummies(full_df['creditRating'], prefix='CR', prefix_sep='_')], axis=1)
full_df.drop('creditRating', 1, inplace=True)

full_df = pd.concat([full_df, pd.get_dummies(full_df['income'], prefix='Inc', prefix_sep='_')], axis=1)
full_df.drop('income', 1, inplace=True)
print(full_df)

remaining_categorical_columns = full_df[['children', 'smartPhone', 'homeOwner', 'creditCard']].columns
full_df = bool_to_binary(full_df, continuous_columns=remaining_categorical_columns)

cont_columns = full_df[['age', 'numHandsets', 'handsetAge', 'avgBill', 'avgMins',
                        'avgrecurringCharge', 'avgOverBundleMins', 'avgRoamCalls', 'callMinutesChangePct',
                        'billAmountChangePct', 'avgReceivedMins', 'avgOutCalls', 'avgInCalls', 'peakOffPeakRatio',
                        'peakOffPeakRatioChangePct', 'avgDroppedCalls', 'lifeTime', 'lastMonthCustomerCareCalls',
                        'numRetentionCalls', 'numRetentionOffersAccepted', 'newFrequentNumbers']].columns

scaler = MinMaxScaler()
full_df[cont_columns] = scaler.fit_transform(full_df[cont_columns])
full_df = memory_management(full_df, continuous_columns=cont_columns)
print(full_df)
full_df.to_csv('postFeatureAdjustments_CustomerChurn-17205389.csv', sep=',', index=False)
train1, test1 = train_test_split(full_df, test_size=0.3)
train1.to_csv('improve_training_CustomerChurn-17205389.csv', sep=',', index=False)
test1.to_csv('improve_test_CustomerChurn-17205389.csv', sep=',', index=False)


train = pd.read_csv('improve_training_CustomerChurn-17205389.csv', sep=',')
print(len(train.columns))
train = memory_management(train, continuous_columns=train.columns)
X_train = train.loc[:, train.columns != 'churn']
y_train = train['churn']
test = pd.read_csv('improve_test_CustomerChurn-17205389.csv', sep=',')
print(len(test.columns))
test = memory_management(test, continuous_columns=test.columns)
X_test = test.loc[:, test.columns != 'churn']
y_test = test['churn'].values
#
#
# ##############
# ##### LOGREG
# ##############
param_grid = {'C': np.arange(0.0001, 0.01, 0.0001),
              'penalty': ['l1', 'l2']}
logreg = LogisticRegression()
logreg_cv = GridSearchCV(logreg, param_grid, cv=5)
logreg_cv.fit(X_train, y_train)
print(logreg_cv.best_params_)
print("best score", logreg_cv.best_score_)
logreg = LogisticRegression(C=logreg_cv.best_params_['C'], penalty=logreg_cv.best_params_['penalty'])
logreg.fit(X_train, y_train)
logreg_acc_score = round(logreg.score(X_train, y_train) * 100, 2)
print("Logreg Score: ", logreg.score(X_train, y_train))
print("***Logistic Regression***")
print("Accuracy Score:", logreg_acc_score)
y_pred_prob = logreg.predict_proba(X_test)
y_pred = logreg.predict(X_test)
print("ROC_AUC Score:")
roc_score = roc_auc_score(y_test, y_pred)
print(roc_score)
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob[:, 1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % roc_score)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Roc Curve')
plt.legend(loc="lower right")
plt.savefig('logreg.png')
#
#
# ##############
# ##### SVM
# ##############
param_grid = {'C': np.arange(0.0001, 0.01, 0.0001),
              'gamma': [1e-3, 1e-4]}
svm = SVC()
svm_cv = GridSearchCV(svm, param_grid, cv=5)
svm_cv.fit(X_train, y_train)
print(svm_cv.best_params_)
print("best score", svm_cv.best_score_)
svm = SVC(C=svm_cv.best_params_['C'], gamma=svm_cv.best_params_['gamma'], probability=True)
a = svm.fit(X_train, y_train)
svm_acc_score = round(svm.score(X_train, y_train) * 100, 2)
print("***SVM***")
print("Accuracy Score:", svm_acc_score)
print("SVM Score: ", svm.score(X_train, y_train))
y_pred_prob = svm.predict_proba(X_test)[:, 1]
y_pred = svm.predict(X_test)
print("ROC_AUC Score:")
roc_score = roc_auc_score(y_test, y_pred)
print(roc_score)
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
plt.figure()
plt.plot(fpr, tpr, label='SVM (area = %0.2f)' % roc_score)
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Roc Curve')
plt.legend(loc="lower right")
plt.savefig('svm.png')
#
#
# ##############
# ##### Decision Tree
# ##############
param_grid = {'max_depth': np.arange(1, 100)}
decision_tree = DecisionTreeClassifier()
decision_tree_cv = GridSearchCV(decision_tree, param_grid, cv=5)
decision_tree_cv.fit(X_train, y_train)
print(decision_tree_cv.best_params_)
print("best score", decision_tree_cv.best_score_)
decision_tree = DecisionTreeClassifier(max_depth=decision_tree_cv.best_params_['max_depth'])
decision_tree.fit(X_train, y_train)
decision_tree_acc_score = round(decision_tree.score(X_train, y_train) * 100, 2)
print("***Decision Tree***")
# Mean accuracy score
print("Decision tree score:", decision_tree.score(X_train, y_train))
print("Accuracy Score:", decision_tree_acc_score)
y_pred_prob = decision_tree.predict_proba(X_test)[:,1]
y_pred = decision_tree.predict(X_test)
roc_score = roc_auc_score(y_test, y_pred)
print("ROC_AUC Score:")
print(roc_score)
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
plt.figure()
plt.plot(fpr, tpr, label='Decision Tree (area = %0.2f)' % roc_score)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Roc Curve')
plt.legend(loc="lower right")
plt.savefig('decisiontree.png')
#
# ##############
# ##### KNN
# ##############
param_grid = {'n_neighbors': np.arange(5, 50)}
knn = KNeighborsClassifier()
knn_cv = GridSearchCV(knn, param_grid, cv=5)
knn_cv.fit(X_train, y_train)
print(knn_cv.best_params_)
print("best score", knn_cv.best_score_)
knn = KNeighborsClassifier(n_neighbors=knn_cv.best_params_['n_neighbors'])
knn.fit(X_train, y_train)
knn_acc_score = round(knn.score(X_train, y_train) * 100, 2)
print("***K Nearest Neighbors***")
print("Accuracy Score:", knn_acc_score)
print("KNN Score:", knn.score(X_train, y_train))
print("ROC_AUC Score:")
y_pred_prob =knn.predict_proba(X_test)[:, 1]
y_pred = knn.predict(X_test)
roc_score = roc_auc_score(y_test, y_pred)
print(roc_score)
fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob)
plt.figure()
plt.plot(fpr, tpr, label='KNN (area = %0.2f)' % roc_score)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Roc Curve')
plt.legend(loc="lower right")
plt.savefig('knn.png')
#
#
# ################################################################################################################
# ##### Survival Algoritmaları
# ##############
train = pd.read_csv('improve_training_CustomerChurn-17205389.csv', sep=',')
print(len(train.columns))
train = memory_management(train, continuous_columns=train.columns)
X_train = train.drop(['churn', 'lifeTime'], axis=1)
y_train = Surv.from_dataframe(event='churn', time='lifeTime', data=train)
test = pd.read_csv('improve_test_CustomerChurn-17205389.csv', sep=',')
# a = test.memory_usage().sum()
test = memory_management(test, continuous_columns=test.columns)
# b = test.memory_usage().sum()
churn = test['churn']
# print(a)
# print(b)
# print(((a-b)/a)*100)
X_test = test.drop(['churn', 'lifeTime'], axis=1)
y_test = Surv.from_dataframe(event='churn', time='lifeTime', data=test)
#
#
# #############
# #### SVM
# #############
param_grid = {'alpha': 2. ** np.arange(-10, 11, 4)}
svm = FastSurvivalSVM()
svm_cv = GridSearchCV(svm, param_grid, cv=5)
svm_cv.fit(X_train, y_train)
print(svm_cv.best_params_)
print("best score", svm_cv.best_score_)
svm = FastSurvivalSVM(alpha=svm_cv.best_params_['alpha'], optimizer='rbtree', tol=1e-6)
svm.fit(X_train, y_train)
svm_acc_score = round(svm.score(X_train, y_train) * 100, 2)
print("***Survival SVM***")
print("Accuracy Score:", svm_acc_score)
print("Survival SVM Score:", svm.score(X_train, y_train))
y_pred = svm.predict(X_test)
times = [item[1] for item in y_test]
va_auc, va_mean_auc = cumulative_dynamic_auc(y_train, y_test, y_pred, np.arange(min(times), max(times), 0.01))
plt.bar(y_test['lifeTime'], va_auc, width=0.7)
plt.axhline(va_mean_auc, linestyle="--")
plt.xlabel("days from enrollment")
plt.ylabel("time-dependent AUC")
plt.grid(True)
plt.show()
